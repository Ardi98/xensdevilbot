package xs.discord.xsbot.commands.music;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import net.dv8tion.jda.api.entities.TextChannel;
import xs.discord.xsbot.commands.CommandContext;
import xs.discord.xsbot.configuration.XSConfig;
import xs.discord.xsbot.manager.ICommand;
import xs.discord.xsbot.musicdata.GuildMusicManager;
import xs.discord.xsbot.musicdata.PlayerManager;
import xs.discord.xsbot.musicdata.TrackScheduler;

public class SkipCommand implements ICommand {
    @Override
    public void handle(CommandContext ctx) {
        TextChannel channel = ctx.getChannel();
        PlayerManager playerManager = PlayerManager.getInstance();
        GuildMusicManager guildMusicManager = playerManager.getGuildMusicManager(ctx.getGuild());
        TrackScheduler scheduler = guildMusicManager.scheduler;
        AudioPlayer player = guildMusicManager.player;

        if(player.getPlayingTrack() == null) {
            channel.sendMessage("Der Musicplayer spielt gerade nichts ab!").queue();

            return;
        }

        try {
            scheduler.nextTrack();
        } catch(Exception e) {
//            System.out.println("Nochmal skippen!");
            scheduler.nextTrack();
        }
        channel.sendMessage("Skippt den aktuellen Song..").queue();
    }

    @Override
    public String getName() {
        return "skip";
    }

    @Override
    public String getHelp() {
        return "Skipt den aktuellen Song\n"+
                "Nutze: '"+ XSConfig.get("PREFIX")+getName()+"'";
    }
}
