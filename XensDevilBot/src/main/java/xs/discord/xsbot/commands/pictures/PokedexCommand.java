package xs.discord.xsbot.commands.pictures;

import me.duncte123.botcommons.messaging.EmbedUtils;
import me.duncte123.botcommons.web.WebUtils;
import net.dv8tion.jda.api.EmbedBuilder;
import xs.discord.xsbot.commands.CommandContext;
import xs.discord.xsbot.configuration.XSConfig;
import xs.discord.xsbot.manager.ICommand;

import java.util.List;

public class PokedexCommand implements ICommand {
    @Override
    public void handle(CommandContext ctx) {
        List<String> args = ctx.getArgs();

        if(args.isEmpty()) {
            ctx.getChannel().sendMessage("Du musst Argumente angeben!").queue();
            return;
        }
        String input = String.join(" ", args);
        try {
            WebUtils.ins.getJSONObject("https://pokeapi.co/api/v2/pokemon/" + input).async((json) -> {
                boolean hasKey = json.has("sprites");
                if (hasKey) {
                    String url = json.get("sprites").get("front_default").asText();
                    EmbedBuilder embed = EmbedUtils.embedImage(url);

                    boolean hasType = json.has("types");
                    if(hasType) {
                        String types = "";
                        for(int i = 0; i<json.get("types").size(); i++) {
                            if(i < 1 && json.get("types").size() > 0) {
                                types = types + json.get("types").get(i).get("type").get("name").asText() + ", ";
                            } else {
                                types = types + json.get("types").get(i).get("type").get("name").asText();
                            }
                        }
                        embed.appendDescription("Type(s): "+types.toUpperCase());
                    }
                    ctx.getChannel().sendMessage(embed.build()).queue();
                }
            });
        } catch(Exception e) {
            ctx.getChannel().sendMessage("Dieses Pokemon gibt es nicht!").queue();
        }
    }

    @Override
    public String getName() {
        return "pkmn";
    }

    @Override
    public String getHelp() {
        return "Gibt Daten über ein Pokemon aus (ENGLISCH)!\n"+
                "Nutze: '"+ XSConfig.get("PREFIX")+getName()+" pokemonname'";
    }
}
