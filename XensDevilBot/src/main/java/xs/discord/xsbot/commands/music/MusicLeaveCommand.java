package xs.discord.xsbot.commands.music;

import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.managers.AudioManager;
import xs.discord.xsbot.commands.CommandContext;
import xs.discord.xsbot.manager.ICommand;

public class MusicLeaveCommand implements ICommand {
    @Override
    public void handle(CommandContext ctx) {
        TextChannel channel = ctx.getChannel();
        AudioManager audioManager = ctx.getGuild().getAudioManager();

        if (!audioManager.isConnected()) {
            channel.sendMessage("Ich bin mit keinem VoiceChannel verbunden").queue();
            return;
        }

        VoiceChannel voiceChannel = audioManager.getConnectedChannel();

        if (!voiceChannel.getMembers().contains(ctx.getMember())) {
            channel.sendMessage("Du musst im selben VoiceChannel wie ich sein, um diesen Befehl richtig zu nutzen").queue();
            return;
        }

        audioManager.closeAudioConnection();
        channel.sendMessage("[XensDevilBot] Ich bin aus dem Channel ausgetreten").queue();
    }

    @Override
    public String getName() {
        return "leave";
    }

    @Override
    public String getHelp() {
        return "Der Bot geht aus dem Channel raus";
    }
}
