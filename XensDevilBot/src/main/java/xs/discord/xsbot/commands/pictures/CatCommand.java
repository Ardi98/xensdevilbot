package xs.discord.xsbot.commands.pictures;

import me.duncte123.botcommons.messaging.EmbedUtils;
import me.duncte123.botcommons.web.WebUtils;
import net.dv8tion.jda.api.EmbedBuilder;
import xs.discord.xsbot.commands.CommandContext;
import xs.discord.xsbot.configuration.XSConfig;
import xs.discord.xsbot.manager.ICommand;

public class CatCommand implements ICommand {
    @Override
    public void handle(CommandContext ctx) {

        WebUtils.ins.scrapeWebPage("https://api.thecatapi.com/api/images/get?format=xml&results_per_page=1").async( (document) -> {
            String url = document.getElementsByTag("url").first().html();
            EmbedBuilder embed = EmbedUtils.embedImage(url);
            ctx.getChannel().sendMessage(embed.build()).queue();
        });



    }

    @Override
    public String getName() {
        return "cat";
    }

    @Override
    public String getHelp() {
        return "Zeigt ein Bild von einer zufaelligen Katze\n"+
                "Nutze: '"+ XSConfig.get("PREFIX")+getName()+"'";
    }
}
