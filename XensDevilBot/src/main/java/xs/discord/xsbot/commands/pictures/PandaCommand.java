package xs.discord.xsbot.commands.pictures;

import me.duncte123.botcommons.messaging.EmbedUtils;
import me.duncte123.botcommons.web.WebUtils;
import net.dv8tion.jda.api.EmbedBuilder;
import xs.discord.xsbot.commands.CommandContext;
import xs.discord.xsbot.configuration.XSConfig;
import xs.discord.xsbot.manager.ICommand;

public class PandaCommand implements ICommand {
    @Override
    public void handle(CommandContext ctx) {
        WebUtils.ins.getJSONObject("https://some-random-api.ml/img/panda").async( (json) -> {
            boolean hasKey = json.has("link");
            if(hasKey) {
                String url = json.get("link").asText();
                EmbedBuilder embed = EmbedUtils.embedImage(url);
                ctx.getChannel().sendMessage(embed.build()).queue();
            }
        });
    }

    @Override
    public String getName() {
        return "panda";
    }

    @Override
    public String getHelp() {
        return "Zeigt ein Bild von einem zufaelligen Panda\n"+
                "Nutze: '"+ XSConfig.get("PREFIX")+getName()+"'";
    }
}
