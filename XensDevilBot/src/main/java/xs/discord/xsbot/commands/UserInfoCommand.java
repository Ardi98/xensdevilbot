package xs.discord.xsbot.commands;

import com.jagrosh.jdautilities.commons.utils.FinderUtil;
import me.duncte123.botcommons.messaging.EmbedUtils;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import xs.discord.xsbot.configuration.XSConfig;
import xs.discord.xsbot.manager.ICommand;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

public class UserInfoCommand implements ICommand {

    @Override
    public void handle(CommandContext ctx) {
        List<String> args = ctx.getArgs();
        TextChannel channel = ctx.getChannel();

        if(args.isEmpty()) {
            channel.sendMessage("Fehlende Argumente, siehe hier: '" + XSConfig.get("PREFIX") + "help " + getName() + "'").queue();
            return;
        }

        String joined = String.join("", args);
        List<User> foundUsers = FinderUtil.findUsers(joined, ctx.getJDA());

        if(foundUsers.isEmpty()) {
            List<Member> foundMembers = FinderUtil.findMembers(joined, ctx.getGuild());
            if(foundMembers.isEmpty()) {
                channel.sendMessage("Keine Nutzer gefunden fuer '"+joined+"'").queue();
                return;
            }
            foundUsers = foundMembers.stream().map(Member::getUser).collect(Collectors.toList());
        }

        User user = foundUsers.get(0);
        Member member = ctx.getGuild().getMember(user);

        MessageEmbed embed = EmbedUtils.getDefaultEmbed()
                .setColor(member.getColor())
                .setThumbnail(user.getEffectiveAvatarUrl().replaceFirst("gif", "png"))
                .addField("Username#Discriminator", String.format("%#s", user), false)
                .addField("Anzeigename", member.getEffectiveName(), false)
                .addField("Nutzer Id + Erwaehnung", String.format("%s (%s)", user.getId(), member.getAsMention()), false)
                .addField("Account erstellt", user.getTimeCreated().format(DateTimeFormatter.RFC_1123_DATE_TIME), false)
                .addField("Beigetreten", member.getTimeJoined().format(DateTimeFormatter.RFC_1123_DATE_TIME), false)
                .addField("Online Status", member.getOnlineStatus().name().toLowerCase().replaceAll("_", " "), false)
                .addField("Bot Account", user.isBot() ? "Yes" : "No", false)
                .build();
        channel.sendMessage(embed).queue();
    }

    @Override
    public String getName() {
        return "userinfo";
    }

    @Override
    public String getHelp() {
        return "Zeigt Informationen ueber einen Nutzer.\n"
                +"Benutze: '"+XSConfig.get("PREFIX")+getName()+" [nutzername/@nutzer/nutzer id]'";
    }
}
