package xs.discord.xsbot.commands.music;

import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.GuildVoiceState;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.managers.AudioManager;
import xs.discord.xsbot.commands.CommandContext;
import xs.discord.xsbot.manager.ICommand;

public class MusicJoinCommand implements ICommand {
    @Override
    public void handle(CommandContext ctx) {
        TextChannel channel = ctx.getChannel();
        AudioManager audioManager = ctx.getGuild().getAudioManager();

        if(audioManager.isConnected()) {
            channel.sendMessage("Der Bot ist bereits mit einem Channel verbunden!").queue();
            return;
        }

        GuildVoiceState memberVoiceState = ctx.getMember().getVoiceState();

        if(!memberVoiceState.inVoiceChannel()) {
            channel.sendMessage("Du musst erst einem VoiceChannel beitreten!").queue();
            return;
        }

        VoiceChannel voiceChannel = memberVoiceState.getChannel();
        Member selfMember = ctx.getGuild().getSelfMember();

        if(!selfMember.hasPermission(voiceChannel, Permission.VOICE_CONNECT)) {
            channel.sendMessage("Ich habe keine Rechte, diesem Channel "+voiceChannel+" beizutreten!").queue();
            return;
        }

        audioManager.openAudioConnection(voiceChannel);
        channel.sendMessage("[XensDevilBot] Trete deinem VoiceChannel bei...").queue();
    }

    @Override
    public String getName() {
        return "join";
    }

    @Override
    public String getHelp() {
        return "Der Bot joint dem Channel";
    }
}
