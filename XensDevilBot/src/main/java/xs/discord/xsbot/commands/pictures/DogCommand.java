package xs.discord.xsbot.commands.pictures;

import me.duncte123.botcommons.messaging.EmbedUtils;
import me.duncte123.botcommons.web.WebUtils;
import net.dv8tion.jda.api.EmbedBuilder;
import xs.discord.xsbot.commands.CommandContext;
import xs.discord.xsbot.configuration.XSConfig;
import xs.discord.xsbot.manager.ICommand;

public class DogCommand implements ICommand {
    @Override
    public void handle(CommandContext ctx) {
        WebUtils.ins.getJSONObject("https://random.dog/woof.json").async( (json) -> {
            boolean hasKey = json.has("url");
            if(hasKey) {
                String url = json.get("url").asText();
                EmbedBuilder embed = EmbedUtils.embedImage(url);
                ctx.getChannel().sendMessage(embed.build()).queue();
            }
        });
    }

    @Override
    public String getName() {
        return "dog";
    }

    @Override
    public String getHelp() {
        return "Zeigt ein Bild von einem zufaelligen Hund\n"+
                "Nutze: '"+ XSConfig.get("PREFIX")+getName()+"'";
    }
}
