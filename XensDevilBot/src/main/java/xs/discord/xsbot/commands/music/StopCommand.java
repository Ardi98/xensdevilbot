package xs.discord.xsbot.commands.music;

import net.dv8tion.jda.api.entities.TextChannel;
import xs.discord.xsbot.commands.CommandContext;
import xs.discord.xsbot.configuration.XSConfig;
import xs.discord.xsbot.manager.ICommand;
import xs.discord.xsbot.musicdata.GuildMusicManager;
import xs.discord.xsbot.musicdata.PlayerManager;

public class StopCommand implements ICommand {
    @Override
    public void handle(CommandContext ctx) {
        PlayerManager playerManager = PlayerManager.getInstance();
        GuildMusicManager musicManager = playerManager.getGuildMusicManager(ctx.getGuild());

        musicManager.scheduler.getQueue().clear();
        musicManager.player.stopTrack();
        musicManager.player.setPaused(false);

        TextChannel channel = ctx.getChannel();
        channel.sendMessage("Stoppt den Musicplayer und entfernt die laufende Abspielwarteschlange!").queue();
    }

    @Override
    public String getName() {
        return "stop";
    }

    @Override
    public String getHelp() {
        return "Stoppt den aktuellen Track!\n"+
                "Nutze: '"+XSConfig.get("PREFIX")+getName()+"'";
    }
}
