package xs.discord.xsbot.commands.music;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo;
import me.duncte123.botcommons.messaging.EmbedUtils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.TextChannel;
import xs.discord.xsbot.commands.CommandContext;
import xs.discord.xsbot.configuration.XSConfig;
import xs.discord.xsbot.manager.ICommand;
import xs.discord.xsbot.musicdata.GuildMusicManager;
import xs.discord.xsbot.musicdata.PlayerManager;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

public class QueueCommand implements ICommand {
    @Override
    public void handle(CommandContext ctx) {
        TextChannel channel = ctx.getChannel();
        PlayerManager playerManager = PlayerManager.getInstance();
        GuildMusicManager guildMusicManager = playerManager.getGuildMusicManager(ctx.getGuild());
        BlockingQueue<AudioTrack> queue = guildMusicManager.scheduler.getQueue();

        if(queue.isEmpty()) {
            channel.sendMessage("Die Warteschlange ist leer!").queue();

            return;
        }

        int trackCount = Math.min(queue.size(), 20);
        List<AudioTrack> tracks = new ArrayList<>(queue);
        EmbedBuilder builder = EmbedUtils.getDefaultEmbed().setTitle("Aktuelle Warteschlange (Gesamt: "+queue.size()+ ")");

        for(int i = 0; i<trackCount; i++) {
            AudioTrack track = tracks.get(i);
            AudioTrackInfo info = track.getInfo();

            builder.appendDescription(String.format("%s - %s\n",info.title,info.author));
        }

        channel.sendMessage(builder.build()).queue();
    }

    @Override
    public String getName() {
        return "queue";
    }

    @Override
    public String getHelp() {
        return "Zeigt die aktuelle Musik-Warteschlange an\n"+
                "Nutze: '"+ XSConfig.get("PREFIX")+getName()+"'";
    }
}
