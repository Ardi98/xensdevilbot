package xs.discord.xsbot.commands.coc;

import me.duncte123.botcommons.messaging.EmbedUtils;
import net.dv8tion.jda.api.EmbedBuilder;
import org.json.JSONObject;
import xs.discord.xsbot.commands.CommandContext;
import xs.discord.xsbot.configuration.XSConfig;
import xs.discord.xsbot.manager.ICommand;

import java.awt.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

public class CocCommand implements ICommand {
    @Override
    public void handle(CommandContext ctx) {

        try {

            List<String> args = ctx.getArgs();

            String key = XSConfig.get("KEY");
            String url;

            if(args.isEmpty()) {
                url = "https://api.clashofclans.com/v1/clans?name=Imperiumclan2";
            } else if(args.size() == 1){
                url = "https://api.clashofclans.com/v1/clans?name="+args.get(0);
            } else {
                String clanname = String.join("-", args);
                url = "https://api.clashofclans.com/v1/clans?name="+clanname;
            }

            //curl -X GET --header 'Accept: application/json' --header "authorization: Bearer <API token>" 'https://api.clashofclans.com/v1/clans?name=Imperiumclan2'

            String[] command = { "curl", "-X", "GET", "--header", "Accept: application/json", "--header", "authorization: Bearer "+key, url};

            ProcessBuilder process = new ProcessBuilder(command);
            Process p = process.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuilder builder = new StringBuilder();
            String line = null;
            while((line = reader.readLine()) != null) {
                builder.append(line);
                builder.append(System.getProperty("line.seperator"));
            }
            String result = builder.toString();
//            System.out.println(result);

            JSONObject object = new JSONObject(result);
            if(object.has("items")) {
//                System.out.println(object.getJSONArray("items").getJSONObject(0).getJSONObject("badgeUrls").get("medium").toString());
//                System.out.println(object.getJSONArray("items").getJSONObject(0).getJSONObject("badgeUrls").get("medium").toString());

                String badge = object.getJSONArray("items").getJSONObject(0).getJSONObject("badgeUrls").get("medium").toString();
                String clanname = object.getJSONArray("items").getJSONObject(0).get("name").toString();
                int members = object.getJSONArray("items").getJSONObject(0).getInt("members");
                int warwins = object.getJSONArray("items").getJSONObject(0).getInt("warWins");
                int warwinstreak = object.getJSONArray("items").getJSONObject(0).getInt("warWinStreak");
                int clanlevel = object.getJSONArray("items").getJSONObject(0).getInt("clanLevel");


                EmbedBuilder embedBuilder = EmbedUtils.embedImage(badge);
                embedBuilder.setColor(Color.ORANGE)
                        .addField("Clanname: ", clanname, false)
                        .addField("ClanLevel: ", String.valueOf(clanlevel), false)
                        .addField("Mitglieder: ", String.valueOf(members), false)
                        .addField("Siege[CK]: ", String.valueOf(warwins), false)
                        .addField("WinStreak: ", String.valueOf(warwinstreak), false);
                ctx.getChannel().sendMessage(embedBuilder.build()).queue();
            }

        } catch(Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public String getName() {
        return "clan";
    }

    @Override
    public String getHelp() {
        return "Zeigt Daten eines Clans an!\n"+
                "Nutze: '"+ XSConfig.get("PREFIX")+getName()+" [clanname]"+"'";
    }
}
