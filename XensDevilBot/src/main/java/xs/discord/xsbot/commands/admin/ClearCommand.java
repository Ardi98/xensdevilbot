package xs.discord.xsbot.commands.admin;

import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import xs.discord.xsbot.commands.CommandContext;
import xs.discord.xsbot.configuration.XSConfig;
import xs.discord.xsbot.manager.ICommand;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class ClearCommand implements ICommand {
    @Override
    public void handle(CommandContext ctx) {
        TextChannel channel = ctx.getChannel();
        Member member = ctx.getMember();
        Member selfMember = ctx.getSelfMember();
        List<String> args = ctx.getArgs();

        if(!member.hasPermission(Permission.MESSAGE_MANAGE)) {
            channel.sendMessage("Du brauchst die \"Manage Messages\" Rechte um diesen Befehl nutzen zu dürfen!").queue();

            return;
        }

        if(!selfMember.hasPermission(Permission.MESSAGE_MANAGE)) {
            channel.sendMessage("Du brauchst die \"Manage Messages\" Rechte um diesen Befehl nutzen zu dürfen!").queue();

            return;
        }

        if(args.isEmpty()) {
            channel.sendMessage("Fehlende Argumente. Nutze: '"+ XSConfig.get("PREFIX")+getName()+" <anzahl>'").queue();

            return;
        }

        int amount;
        String arg = args.get(0);

        try {
            amount = Integer.parseInt(arg);
        } catch(NumberFormatException nfe) {
            channel.sendMessageFormat("'%s' ist keine gültige Zahl", arg).queue();
            return;
        }

        if(amount < 2 || amount > 100) {
            channel.sendMessage("Die Anzahl muss zwischen mindestens 2 und höchstens 100 liegen!").queue();
            return;
        }

        channel.getIterableHistory()
                .takeAsync(amount)
                .thenApplyAsync((messages) -> {
                    List<Message> goodMessages = messages.stream()
                            .filter((m) -> m.getTimeCreated().isBefore(
                                    OffsetDateTime.now().plus(2, ChronoUnit.WEEKS)
                            ))
                            .collect(Collectors.toList());

                    channel.purgeMessages(goodMessages);

                    return goodMessages.size();
                })
                .whenCompleteAsync((count, thr) -> channel.sendMessageFormat("'%d' Nachrichten entfernt", count).queue((message) -> message.delete().queueAfter(2, TimeUnit.SECONDS)))
                .exceptionally((thr) -> {
                    String cause = "";

                    if (thr.getCause() != null) {
                        cause = " caused by: " + thr.getCause().getMessage();
                    }

                    channel.sendMessageFormat("Error: %s%s", thr.getMessage(), cause).queue();

                    return 0;
                });
    }

    @Override
    public String getName() {
        return "clear";
    }

    @Override
    public String getHelp() {
        return "Löscht Zeilen im Textchannel!\n"+
                "Nutze: '";
    }
}
