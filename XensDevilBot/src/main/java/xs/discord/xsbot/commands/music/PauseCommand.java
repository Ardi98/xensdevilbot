package xs.discord.xsbot.commands.music;

import net.dv8tion.jda.api.entities.TextChannel;
import xs.discord.xsbot.commands.CommandContext;
import xs.discord.xsbot.configuration.XSConfig;
import xs.discord.xsbot.manager.ICommand;
import xs.discord.xsbot.musicdata.GuildMusicManager;
import xs.discord.xsbot.musicdata.PlayerManager;

public class PauseCommand implements ICommand {
    @Override
    public void handle(CommandContext ctx) {
        PlayerManager playerManager = PlayerManager.getInstance();
        GuildMusicManager musicManager = playerManager.getGuildMusicManager(ctx.getGuild());
        TextChannel channel = ctx.getChannel();
        if(musicManager.player.isPaused()) {
            musicManager.player.setPaused(false);
            channel.sendMessage("Aktueller Track \""+musicManager.player.getPlayingTrack().getInfo().title+"\" läuft nun weiter!").queue();
            return;
        }
        musicManager.player.setPaused(true);
        channel.sendMessage("Aktueller Track \""+musicManager.player.getPlayingTrack().getInfo().title+"\" wurde pausiert!").queue();
    }

    @Override
    public String getName() {
        return "pause";
    }

    @Override
    public String getHelp() {
        return "Pausiert den aktuellen Track oder laesst den aktuellen Track weiterlaufen!\n"+
                "Nutze: '"+ XSConfig.get("PREFIX")+getName()+"'";
    }
}
