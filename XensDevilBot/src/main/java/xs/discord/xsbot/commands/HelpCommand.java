package xs.discord.xsbot.commands;

import net.dv8tion.jda.api.entities.TextChannel;
import xs.discord.xsbot.configuration.XSConfig;
import xs.discord.xsbot.manager.CommandManager;
import xs.discord.xsbot.manager.ICommand;

import java.util.Arrays;
import java.util.List;

public class HelpCommand implements ICommand {

    private final CommandManager manager;

    public HelpCommand(CommandManager manager) {
        this.manager = manager;
    }

    @Override
    public void handle(CommandContext ctx) {
        List<String> args = ctx.getArgs();
        TextChannel channel = ctx.getChannel();

        if(args.isEmpty()) {
            StringBuilder builder = new StringBuilder();
            builder.append("Liste von Befehlen\n");

            manager.getCommands().stream().map(ICommand::getName).forEach((it)->builder.append('`').append(XSConfig.get("PREFIX")).append(it).append("`\n"));
            channel.sendMessage(builder.toString()).queue();
            return;
        }

        String search = args.get(0);
        ICommand command = manager.getCommand(search);

        if(command == null) {
            channel.sendMessageFormat("Nichts gefunden fuer "+search).queue();
            return;
        }

        channel.sendMessageFormat(command.getHelp()).queue();
    }

    @Override
    public String getName() {
        return "hilfe";
    }

    @Override
    public String getHelp() {
        return "Zeigt eine Liste mit Befehlen des XensDevilBots\n"+
                "Benutze: '!xs!hilfe [befehl]'";
    }

    @Override
    public List<String> getAliases() {
        return Arrays.asList("befehle", "cmds");
    }
}
