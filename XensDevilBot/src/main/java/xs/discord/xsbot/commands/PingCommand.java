package xs.discord.xsbot.commands;

import net.dv8tion.jda.api.JDA;
import xs.discord.xsbot.manager.ICommand;

public class PingCommand implements ICommand {
    @Override
    public void handle(CommandContext ctx) {
        JDA jda = ctx.getJDA();
        jda.getRestPing().queue((ping) -> ctx.getChannel().sendMessageFormat("Ping: %sms\nWS ping: %sms",ping, jda.getGatewayPing()).queue());
    }

    @Override
    public String getHelp() {
        return "Zeigt den aktuellen Ping des Bots auf dem XensDevil Server";
    }

    @Override
    public String getName() {
        return "ping";
    }
}
