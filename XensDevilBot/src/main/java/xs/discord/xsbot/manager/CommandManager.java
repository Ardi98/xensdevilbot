package xs.discord.xsbot.manager;

import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import xs.discord.xsbot.commands.CommandContext;
import xs.discord.xsbot.commands.HelpCommand;
import xs.discord.xsbot.commands.PingCommand;
import xs.discord.xsbot.commands.UserInfoCommand;
import xs.discord.xsbot.commands.admin.ClearCommand;
import xs.discord.xsbot.commands.coc.CocCommand;
import xs.discord.xsbot.commands.music.*;
import xs.discord.xsbot.commands.pictures.CatCommand;
import xs.discord.xsbot.commands.pictures.DogCommand;
import xs.discord.xsbot.commands.pictures.PandaCommand;
import xs.discord.xsbot.commands.pictures.PokedexCommand;
import xs.discord.xsbot.configuration.XSConfig;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class CommandManager {

    private final List<ICommand> commands = new ArrayList<>();

    public CommandManager() {
        addCommand(new PingCommand());
        addCommand(new HelpCommand(this));
        addCommand(new UserInfoCommand());
        addCommand(new MusicJoinCommand());
        addCommand(new MusicLeaveCommand());
        addCommand(new PlayCommand());
        addCommand(new StopCommand());
        addCommand(new PauseCommand());
        addCommand(new ClearCommand());
        addCommand(new QueueCommand());
        addCommand(new SkipCommand());
        addCommand(new NowPlayingCommand());
        addCommand(new CatCommand());
        addCommand(new DogCommand());
        addCommand(new PandaCommand());
        addCommand(new PokedexCommand());
        addCommand(new CocCommand());
    }

    private void addCommand(ICommand cmd) {
        boolean nameFound = this.commands.stream().anyMatch((it) -> it.getName().equalsIgnoreCase(cmd.getName()));
        if(nameFound) {
            throw new IllegalArgumentException("Es existiert schon ein Befehl mit diesem Namen!");
        }

        commands.add(cmd);
    }

    public List<ICommand> getCommands() {
        return commands;
    }

    @Nullable
    public ICommand getCommand(String search) {
        String searchLowerCase = search.toLowerCase();

        for(ICommand cmd : this.commands) {
            if(cmd.getName().equals(searchLowerCase) || cmd.getAliases().contains(searchLowerCase)) {
                return cmd;
            }
        }
        return null;
    }

    public void handle(GuildMessageReceivedEvent event) {
        String[] split = event.getMessage().getContentRaw()
                .replaceFirst("(?i)"+ Pattern.quote(XSConfig.get("PREFIX")),"")
                .split("\\s+");
        String invoke = split[0].toLowerCase();
        ICommand cmd = this.getCommand(invoke);

        if(cmd != null) {
            event.getChannel().sendTyping().queue();
            List<String> args = Arrays.asList(split).subList(1, split.length);
            CommandContext ctx = new CommandContext(event, args);
            cmd.handle(ctx);
        }
    }

}
