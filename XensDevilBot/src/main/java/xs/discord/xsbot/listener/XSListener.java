package xs.discord.xsbot.listener;

import me.duncte123.botcommons.BotCommons;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xs.discord.xsbot.configuration.XSConfig;
import xs.discord.xsbot.manager.CommandManager;

import javax.annotation.Nonnull;

public class XSListener extends ListenerAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(XSListener.class);
    private final CommandManager manager = new CommandManager();

    @Override
    public void onReady(@Nonnull ReadyEvent event) {
        LOGGER.info("{} ist nun eingeschaltet!", event.getJDA().getSelfUser().getAsTag());
    }

    @Override
    public void onGuildMessageReceived(@Nonnull GuildMessageReceivedEvent event) {

        User user = event.getAuthor();
        if(user.isBot() || event.isWebhookMessage()) {
            return;
        }

        String prefix = XSConfig.get("PREFIX");
        String raw = event.getMessage().getContentRaw();

        if(raw.equalsIgnoreCase(prefix+"shutdown") && user.getId().equals(XSConfig.get("OWNER_ID"))) {
            LOGGER.info("XensDevilBot abgeschaltet!");
            event.getJDA().shutdown();
            BotCommons.shutdown(event.getJDA());
            System.exit(0);
            return;
        }

        if(raw.startsWith(prefix)) {
            manager.handle(event);
        }
    }
}
