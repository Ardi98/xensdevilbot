package xs.discord.xsbot.main;

import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import xs.discord.xsbot.configuration.XSConfig;
import xs.discord.xsbot.listener.XSListener;

import javax.security.auth.login.LoginException;

public class XSBot {

    private XSBot() throws LoginException {
        JDABuilder builder = JDABuilder.createDefault(XSConfig.get("TOKEN"))
                .addEventListeners(new XSListener())
                .setActivity(Activity.watching("XensDevilServer"));

        builder.build();

//        new JDABuilder().setToken(XSConfig.get("TOKEN"))
//                .addEventListeners(new XSListener())
//                .setActivity(Activity.watching("XensDevilServer"))
//                .build();
    }

    public static void main(String[] args) throws LoginException {
        new XSBot();
    }

}
